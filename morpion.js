/**
 * code Javascript du projet Morpion en ligne
 */
var nbX=0;
var nbO=0;
$(document).ready(function(){
$("#tableMorpion td").addClass("coup");
$("#voirAide").click(voirAide);
$("#detailAide").hide();
$("#raz").click(function(){
$("#tableMorpion td").html("?").removeClass("x").removeClass("o");
$("#tableMorpion td").css('cursor','pointer');
$("#tableMorpion td").unbind('click').click(setX);
$("#prochain").removeClass("o").addClass("x").html("X");
$("#resultat").html("<i>Partie en cours...</i>").removeClass("o");
$("#suivi").append('<td colspan="2" id="score" style="border:solid 1px#dddddd"></td>');
updateScore();
});
});
function setX() {
if ( ! $(this).hasClass('o') && ! $(this).hasClass('x') ) {
$(this).addClass("x").html("X").css('cursor','wait');
$("#tableMorpion td").unbind('click').click(setO);
$("#prochain").removeClass("x").addClass("o").html("O");
checkGagnant();
}
}
function setO() {
if ( ! $(this).hasClass('o') && ! $(this).hasClass('x') ) {
$(this).addClass("o").html("O").css('cursor','wait');
$("#tableMorpion td").unbind('click').click(setX);
$("#prochain").removeClass("o").addClass("x").html("X");
checkGagnant();
}
}
function checkGagnant() {
var isCaseVide = false;
var t = new Array(9);
$("#tableMorpion td").each(function(i) {
if ( $(this).hasClass('o') ) {
t[i] = 1;
} else if ( $(this).hasClass('x') ) {
t[i] = -1;
} else {
t[i] = 0;
isCaseVide = true;
}
});
// grille pleine
if (!isCaseVide) {
$("#resultat").html("<b>Partie terminée</b>").addClass("o");
}
// victoire de o
if ( ( t[4]==1 && ( t[0]+t[8]==2 || t[1]+t[7]==2 || t[2]+t[6]==2 ||
t[3]+t[5]==2 ) )
|| t[0]+t[1]+t[2]==3 || t[0]+t[3]+t[6]==3 || t[2]+t[5]+t[8]==3 ||
t[6]+t[7]+t[8]==3
)
{
$("#resultat").html("<b>Victoire de O</b>").addClass("o");
$("#tableMorpion td").unbind('click');
nbO++;
updateScore();
}
// victoire de x
if ( ( t[4]==-1 && ( t[0]+t[8]==-2 || t[1]+t[7]==-2 || t[2]+t[6]==-2 ||
t[3]+t[5]==-2 ) )
|| t[0]+t[1]+t[2]==-3 || t[0]+t[3]+t[6]==-3 || t[2]+t[5]+t[8]==-3
|| t[6]+t[7]+t[8]==-3
)
{
$("#resultat").html("<b>Victoire de X</b>").addClass("x");
$("#tableMorpion td").unbind('click');
nbX++;
updateScore();
}
}
function updateScore() {
$("#score").html("<span class='x'>X = "+nbX+
 "</span> &nbsp; | <span class='o'>O = "+nbO+"</span>");
}
function voirAide() {
$("#detailAide").toggle();
}

