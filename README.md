
## Objective of GIT project

The objective of this GIT project is to develop a online game
named **MORPION**.

## Code structure

At the moment the entire code is stored in the main 

- [Quick start](#quick-start)
- [Status](#status)
- [What's included](#whats-included)
- [Bugs and feature requests](#bugs-and-feature-requests)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [Community](#community)
- [Versioning](#versioning)
- [Creators](#creators)
- [Thanks](#thanks)
- [Copyright and license](#copyright-and-license)


## Bugs and feature requests

None at the moment.

## Documentation



## Running documentation locally


## Documentation for previous releases

- For v2.3.2: <https://getbootstrap.com/2.3.2/>
- For v3.3.x: <https://getbootstrap.com/docs/3.3/>
- For v4.0.x: <https://getbootstrap.com/docs/4.0/>

[Previous releases](https://github.com/twbs/bootstrap/releases) and their documentation are also available for download.

## Community

Desperate GIT training group.

## Versioning

Version 1

## Creators

Robert Schoetter

## Thanks

Thanks to nobody!

## Link to project tutorial

The project tutorial can be downloaded here: [Morpion tutorial](www.algofa.com/git/projet/).
